// [Section] JSON Objects
	/*
		- JSON stands for JavaScript Object Notation
		- JSON is also used in other programming languages hence the name JSON
		- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects.
		- JSON is used for serializing different data types

		Syntax:
			{
				"propertyA": "valueB",
				"propertyB": "valueB"
			}
	*/

		// JSON Object
			/*{
				"city": "Quezon City",
				"province": "Metro Manila",
				"country": "Philippines"
			}*/

		// JSON Arrays
			/*[
				{
					"city": "Quezon City",
					"province": "Metro Manila",
					"country": "Philippines",
				}

				{
					"city": "Manila City",
					"province": "Metro Manila",
					"country": "Philippines"
				}
			]*/

// [Section] JSON Methods
	// The JSON object contains method for parsing and converting data into stringified JSON

		let batchesArr = [
							{
								batchName: "Batch X"
								},
							{
								batchName: "Batch Y"
								}
							]
		
		console.log("This is the original array:")
		console.log(batchesArr);
								
	// Stringify Method
		// This is used to convert JavaScript objects into a string
			console.log("Result from STRINGIFY Method:")
			let stringBatchesArr = JSON.stringify(batchesArr);
			console.log(stringBatchesArr);
			console.log(typeof stringBatchesArr);


			let data = JSON.stringify({
				name: 'John',
				address: {
					city: 'Manila',
					country: 'Philippines'
				}
			})
			console.log(data);

// [Section] Using Stringify Method with Variables
	// When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	// User details
	/*let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = prompt("How young are you?");
	let address = {
		city: prompt("Which city do you live in?"),
		country: prompt("Which country does your city address belong to?")
	}

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	})
		console.log(otherData);*/

	/*or*/
	/*let otherData = JSON.stringify({
		firstName,
		lastName,
		age,
		address,
	})
		console.log(otherData);*/


// [Section] Converting Stringify JSON into JavaScript Objects
	/*
		- Objects are common data types used in application because of the complex data structures that can be created out of them.
		- Information is commonly sent to applications in stringified JSON, then converted back into objects
		- This happends both for sending information to a backend application and sending information back to a frontend application
	*/

	let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
	let parseBatchesJSON = JSON.parse(batchesJSON);
	
	console.log(parseBatchesJSON);
	console.log(parseBatchesJSON[0]);


	let stringifiedObject = `{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}`

	console.log(JSON.parse(stringifiedObject));